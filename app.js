if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}
const express = require('express');
const expressLayout = require('express-ejs-layouts');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const fileUpload = require('express-fileupload');
const moment = require('moment');

const app = express();

// Passport config
require('./config/passport')(passport);

app.use(express.static(__dirname + '/public'));

// EJS
app.use(expressLayout);
app.set('view engine', 'ejs');

// Bodyparser
app.use(express.urlencoded({ extended: false }));

// Fileupload middleware
app.use(
  fileUpload({
    limits: { fileSize: 10 * 1024 * 1024 }
  })
);

// Express session
app.use(
  session({
    secret: 'anna best girl',
    resave: true,
    saveUninitialized: true
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Set moment's locale
moment.locale('vi');

// Global vars
app.locals.moment = moment;
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});

// Routes
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/products', require('./routes/products'));
app.use('/requests', require('./routes/requests'));
app.use('/categories', require('./routes/category'));

app.listen(process.env.PORT, console.log(`Server started on ${process.env.PORT}`));
