const Category = require('../models/category');

module.exports = {
  addCategory: (name, root) => {
    return Category.addCategory(name, root);
  },
  getAllCategories: () => {
    return Category.all();
  },
  getCategories: async () => {
    const categories = await Category.all();
    let categoriesList = [];
    for (let i = 0; i < categories.length; i++) {
      if (!categories[i].root) {
        categories[i].childs = [];
        categoriesList.push(categories[i]);
      } else {
        const rootIndex = categoriesList.findIndex(c => c.id === categories[i].root);
        categoriesList[rootIndex].childs.push(categories[i]);
      }
    }
    return categoriesList;
  },
  updateCategory: (id, name, root) => {
    return Category.updateCategory(id, name, root);
  },
  deleteCategory: id => {
    return Category.deleteCategory(id);
  }
};
