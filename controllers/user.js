const User = require('../models/user');

module.exports = {
  getAllSimple: () => {
    return User.allSimple();
  },
  getByEmail: email => {
    return User.byEmail(email);
  },
  getById: id => {
    return User.byId(id);
  },
  getByProductId: id => {
    return User.byProductId(id);
  },
  newUser: (name, email, password, address, role = 'bidder') => {
    return User.addUser(name, email, password, address, role);
  },
  updateUser: (id, name, password, address) => {
    return User.updateUser(id, name, password, address);
  },
  updateUserSimple: (id, name, email, address) => {
    return User.updateUserSimple(id, name, email, address);
  },
  updateUserRole: (id, role) => {
    return User.updateUserRole(id, role);
  }
};
