const Watchlist = require('../models/watchlist');

module.exports = {
  getWatchlistByUserIdSimple: userId => {
    return Watchlist.byUserIdSimple(userId);
  },
  addItem: (userId, productId) => {
    return Watchlist.addItem(userId, productId);
  },
  removeItem: (userId, productId) => {
    return Watchlist.removeItem(userId, productId);
  }
};
