const Banlist = require('../models/banlist');

module.exports = {
  getBanlistByProductId: async productId => {
    return Banlist.allByProductId(productId);
  },
  checkBanned: async (userId, productId) => {
    return Banlist.checkBanned(userId, productId);
  },
  banBidder: (userId, productId) => {
    return Banlist.addItem(userId, productId);
  }
};
