const Bid = require('../models/bid');
const Product = require('../models/product');
const Banlist = require('../models/banlist');

module.exports = {
  addBid: async (userId, productId, value) => {
    const [highestBid, productItem, isBanned] = await Promise.all([
      Bid.maxApprovedByProductId(productId),
      Product.byId(productId),
      Banlist.checkBanned(userId, productId)
    ]);
    let lowestBidPossible = productItem.startingPrice + productItem.increment;
    if (!isBanned) {
      if (highestBid != undefined) lowestBidPossible = highestBid.value + productItem.increment;
      if (value >= lowestBidPossible) {
        const bid = await Bid.addBid(userId, productId, value);
        return bid;
      }
      return { err: 'Giá quá thấp.' };
    }
    return { err: 'Không được quyền đấu giá sản phẩm này' };
  },
  getBidById: id => {
    return Bid.byId(id);
  },
  getMaxApprovedByProductId: productId => {
    return Bid.maxApprovedByProductId(productId);
  },
  getBidsByProductId: productId => {
    return Bid.allByProductId(productId);
  },
  getApprovedByBidderId: bidderId => {
    return Bid.allApprovedByBidderId(bidderId);
  },
  approveBid: id => {
    return Bid.updateBidAprroval(id, true);
  },
  disapproveBid: id => {
    return Bid.updateBidAprroval(id, false);
  },
  disapproveAllBid: (bidderId, productId) => {
    return Bid.updateAllBidApproval(bidderId, productId, false);
  },
  approveAllBidWithMax: (bidderId, productId, value) => {
    return Bid.updateAllBidApprovalWithMax(bidderId, productId, true, value);
  }
};
