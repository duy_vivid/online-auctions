const Product = require('../models/product');
const Banlist = require('../models/banlist');

module.exports = {
  newProduct: (
    name,
    image,
    sideImages,
    price,
    closedDate,
    description,
    increment,
    startingPrice,
    sellerId,
    categoryId
  ) => {
    return Product.addProduct(
      name,
      image,
      sideImages,
      price,
      closedDate,
      description,
      increment,
      startingPrice,
      sellerId,
      categoryId
    );
  },
  getProductById: id => {
    return Product.byId(id);
  },
  getProductNameById: id => {
    return Product.nameById(id);
  },
  getProductByWinnerId: winnerId => {
    return Product.byWinnerId(winnerId);
  },
  getProductsBySellerId: sellerId => {
    return Product.bySellerId(sellerId);
  },
  getSoldProductsBySellerId: sellerId => {
    return Product.allSoldBySellerId(sellerId);
  },
  getProductsByCategoryId: (categoryId, order, page, perPage) => {
    return Product.byCatId(categoryId, order, page, perPage);
  },
  getTopProducts: (order, limit) => {
    return Product.top(order, limit);
  },
  getProductByIdSimple: productId => {
    return Product.simpleById(productId);
  },
  searchProducts: (categoryId, order, page, perPage, keywords) => {
    return Product.search(categoryId, order, page, perPage, keywords);
  },
  updateDescription: (productId, description) => {
    return Product.updateDescription(productId, description);
  },
  setWinner: async (productId, winnerId, finalPrice) => {
    const isBanned = await Banlist.checkBanned(winnerId, productId);
    if (!isBanned) {
      Product.setWinner(productId, winnerId, finalPrice);
      return {};
    }
    return { err: 'Không được quyền đấu giá sản phẩm này' };
  }
};
