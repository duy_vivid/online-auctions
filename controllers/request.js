const Request = require('../models/request');

module.exports = {
  getRequestsByUserId: async userId => {
    return Request.allByUserId(userId);
  },
  getUnapprovedRequests: async () => {
    return Request.allUnapproved();
  },
  newRequest: async userId => {
    return Request.addRequest(userId);
  },
  updateRequest: async (userId, approved) => {
    return Request.updateRequest(userId, approved);
  }
};
