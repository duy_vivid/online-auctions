const pool = require('../utils/db');

const tbName = 'Watchlist';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}";`;
    const result = await pool.query(sql);
    return result.rows;
  },
  byUserIdSimple: async userId => {
    const sql = `SELECT "productId"::int FROM "${tbName}" WHERE "userId" = $1;`;
    const result = await pool.query(sql, [parseInt(userId)]);
    return result.rows;
  },
  addItem: async (userId, productId) => {
    const sql = `INSERT INTO "${tbName}" ("userId", "productId") VALUES ($1, $2) RETURNING *;`;
    const result = await pool.query(sql, [parseInt(userId), parseInt(productId)]);
    return result.rows[0];
  },
  removeItem: async (userId, productId) => {
    const sql = `DELETE FROM "${tbName}" WHERE "userId" = $1 AND "productId" = $2 RETURNING *;`;
    const result = await pool.query(sql, [userId, productId]);
    return result.rows[0];
  }
};
