const pool = require('../utils/db');

const tbName = 'Request';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}" ORDER BY approved;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  allByUserId: async userId => {
    const sql = `SELECT * FROM "${tbName}" WHERE "userId" = $1 ORDER BY approved;`;
    const result = await pool.query(sql, [parseInt(userId)]);
    return result.rows;
  },
  allUnapproved: async () => {
    const sql = `SELECT "userId", date, approved, name, email FROM "${tbName}" JOIN "User" ON "Request"."userId" = "User".id WHERE approved IS NULL ORDER BY DATE;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  addRequest: async userId => {
    const now = new Date();
    const sql = `INSERT INTO "${tbName}" ("userId", date) VALUES ($1, $2) RETURNING *;`;
    const result = await pool.query(sql, [userId, now]);
    return result.rows[0];
  },
  updateRequest: async (userId, approved) => {
    const sql = `UPDATE "${tbName}" SET approved = $1 WHERE "userId" = $2 RETURNING *;`;
    const result = await pool.query(sql, [approved, userId]);
    return result.rows[0];
  },
  deleteRequest: async userId => {
    const sql = `DELETE FROM "${tbName}" WHERE "userId" = $1 RETURNING *;`;
    const result = await pool.query(sql, [userId]);
    return result.rows[0];
  }
};
