const pool = require('../utils/db');
const query = require('../utils/query');

const tbName = 'Product';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}";`;
    const result = await pool.query(sql);
    return result.rows;
  },
  simpleById: async id => {
    const sql = `SELECT id::int, description, "sellerId"::int FROM "${tbName}" WHERE id = $1;`;
    const result = await pool.query(sql, [parseInt(id)]);
    return result.rows[0];
  },
  allByCatId: async id => {
    const sql = `SELECT * FROM "${tbName}" WHERE "categoryId" = $1;`;
    const result = await pool.query(sql, [parseInt(id)]);
    return result.rows;
  },
  byCatId: async (categoryId, order, page, perPage) => {
    const orderQuery = query.order(order);
    const [limit, offset] = query.paginate(perPage, page);
    const sql = `SELECT id, "${tbName}".name, image, price, "submittedDate", "closedDate", increment, "startingPrice", bids.value, bids.name AS "bidderName", CASE WHEN bids.counts IS NULL THEN 0 ELSE bids.counts END AS counts, CASE WHEN value IS NULL THEN "startingPrice" + increment ELSE value + increment END AS "lowestPossible", count(*) OVER() AS full_count FROM "${tbName}" LEFT JOIN (SELECT DISTINCT ON ("Bid"."productId") "Bid"."productId", value, "bidderId", name, counts FROM "Bid" JOIN (SELECT COUNT(id) AS counts, "productId" FROM "Bid" WHERE approved = true GROUP BY "productId") AS "Bid2" ON "Bid"."productId" = "Bid2"."productId" JOIN "User" ON "bidderId" = "User".id WHERE approved = true ORDER BY "productId", value DESC) AS bids ON "${tbName}".id = bids."productId" WHERE "winnerId" IS NULL AND NOW() < "closedDate" AND "categoryId" IN (${categoryId.join(
      ','
    )}) ${orderQuery} LIMIT ${limit} OFFSET ${offset};`;
    const result = await pool.query(sql);
    return result.rows;
  },
  top: async (order, limit) => {
    const orderQuery = query.order(order);
    const sql = `SELECT id, "${tbName}".name, image, price, "submittedDate", "closedDate", increment, "startingPrice", bids.value, bids.name AS "bidderName", CASE WHEN bids.counts IS NULL THEN 0 ELSE bids.counts END AS counts, CASE WHEN value IS NULL THEN "startingPrice" + increment ELSE value + increment END AS "lowestPossible", count(*) OVER() AS full_count FROM "${tbName}" LEFT JOIN (SELECT DISTINCT ON ("Bid"."productId") "Bid"."productId", value, "bidderId", name, counts FROM "Bid" JOIN (SELECT COUNT(id) AS counts, "productId" FROM "Bid" WHERE approved = true GROUP BY "productId") AS "Bid2" ON "Bid"."productId" = "Bid2"."productId" JOIN "User" ON "bidderId" = "User".id WHERE approved = true ORDER BY "productId", value DESC) AS bids ON "${tbName}".id = bids."productId" WHERE "winnerId" IS NULL AND NOW() < "closedDate" ${orderQuery} LIMIT ${limit};`;
    const result = await pool.query(sql);
    return result.rows;
  },
  search: async (categoryId, order, page, perPage, keywords) => {
    const orderQuery = query.order(order);
    const [limit, offset] = query.paginate(perPage, page);
    const sql = `SELECT id, "${tbName}".name, image, price, "submittedDate", "closedDate", increment, "startingPrice", bids.value, bids.name AS "bidderName", CASE WHEN bids.counts IS NULL THEN 0 ELSE bids.counts END AS counts, CASE WHEN value IS NULL THEN "startingPrice" + increment ELSE value + increment END AS "lowestPossible", count(*) OVER() AS full_count FROM "${tbName}" LEFT JOIN (SELECT DISTINCT ON ("Bid"."productId") "Bid"."productId", value, "bidderId", name, counts FROM "Bid" JOIN (SELECT COUNT(id) AS counts, "productId" FROM "Bid" WHERE approved = true GROUP BY "productId") AS "Bid2" ON "Bid"."productId" = "Bid2"."productId" JOIN "User" ON "bidderId" = "User".id WHERE approved = true ORDER BY "productId", value DESC) AS bids ON "${tbName}".id = bids."productId" WHERE "winnerId" IS NULL AND NOW() < "closedDate" AND "categoryId" IN (${categoryId.join(
      ','
    )}) AND document_vectors @@ plainto_tsquery($1) ${orderQuery} LIMIT ${limit} OFFSET ${offset};`;
    const result = await pool.query(sql, [keywords]);
    return result.rows;
  },
  allSoldBySellerId: async sellerId => {
    const sql = `SELECT "${tbName}".id, "${tbName}".name, "User".name AS "winnerName", "finalPrice" FROM "${tbName}" JOIN "User" ON "winnerId" = "User".id WHERE "sellerId" = $1;`;
    const result = await pool.query(sql, [parseInt(sellerId)]);
    return result.rows;
  },
  addProduct: async (
    name,
    image,
    sideImages,
    price,
    closedDate,
    description,
    increment,
    startingPrice,
    sellerId,
    categoryId
  ) => {
    const submittedDate = new Date();
    const sql = `INSERT INTO "${tbName}" (name, image, "sideImages", price, "submittedDate", "closedDate", description, increment, "startingPrice", "sellerId", "categoryId", document_vectors) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, (to_tsvector($12) || to_tsvector($13))) RETURNING id::int, name, price::float, "submittedDate", "closedDate", description, increment::float, "startingPrice"::float, "sellerId"::int, "categoryId"::int;`;
    const result = await pool.query(sql, [
      name,
      image,
      sideImages,
      parseFloat(price),
      submittedDate,
      closedDate,
      description,
      parseFloat(increment),
      parseFloat(startingPrice),
      parseInt(sellerId),
      parseInt(categoryId),
      name,
      description
    ]);
    return result.rows[0];
  },
  nameById: async id => {
    const sql = `SELECT name FROM "${tbName}" WHERE id = $1;`;
    const result = await pool.query(sql, [parseInt(id)]);
    return result.rows[0];
  },
  byId: async id => {
    const sql = `SELECT "${tbName}".*, "User".name AS "sellerName", "Buyer".name AS "winnerName" FROM "${tbName}" JOIN "User" ON "${tbName}"."sellerId" = "User".id LEFT JOIN "User" AS "Buyer" ON "${tbName}"."winnerId" = "Buyer".id WHERE "${tbName}".id = $1;`;
    const result = await pool.query(sql, [parseInt(id)]);
    return result.rows[0];
  },
  byWinnerId: async winnerId => {
    const sql = `SELECT "${tbName}".id, "${tbName}".name, "${tbName}"."finalPrice" FROM "${tbName}" WHERE "winnerId" = $1;`;
    const result = await pool.query(sql, [parseInt(winnerId)]);
    return result.rows;
  },
  bySellerId: async sellerId => {
    const sql = `SELECT id, name, current, "startingPrice", "closedDate" FROM "${tbName}" LEFT JOIN (SELECT MAX(value) AS current, "productId" FROM "Bid" WHERE approved = true GROUP BY "productId") AS "myProdBids" ON id = "myProdBids"."productId" WHERE "sellerId" = $1 AND NOW() < "closedDate" AND "winnerId" IS NULL;`;
    const result = await pool.query(sql, [parseInt(sellerId)]);
    return result.rows;
  },
  updateProduct: async (
    id,
    name,
    image,
    sideImages,
    price,
    closedDate,
    description,
    increment,
    startingPrice,
    sellerId,
    categoryId
  ) => {
    const sql = `UPDATE "${tbName}" SET (name, image, "sideImages", price, "closedDate", description, increment, "startingPrice", "sellerId", "categoryId", document_vectors) = ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, (to_tsvector($11) || to_tsvector($12))) WHERE id = $13 RETURNING id::int, name, price::float, "submittedDate", "closedDate", description, increment::float, "startingPrice"::float, "sellerId"::int, "categoryId"::int;`;
    const result = await pool.query(sql, [
      name,
      image,
      sideImages,
      parseFloat(price),
      closedDate,
      description,
      parseFloat(increment),
      parseFloat(startingPrice),
      parseInt(sellerId),
      parseInt(categoryId),
      name,
      description,
      id
    ]);
    return result.rows[0];
  },
  updateDescription: async (id, description) => {
    const sql = `UPDATE "${tbName}" SET description = $1 WHERE id = $2 RETURNING id::int;`;
    const result = await pool.query(sql, [description, parseInt(id)]);
    return result.rows[0];
  },
  setWinner: async (productId, winnerId, finalPrice) => {
    const sql = `UPDATE "${tbName}" SET ("winnerId", "finalPrice") = ($1, $2) WHERE id = $3;`;
    const result = await pool.query(sql, [parseInt(winnerId), parseFloat(finalPrice), parseInt(productId)]);
    return result.rows[0];
  },
  deleteProduct: async id => {
    const sql = `DELETE FROM "${tbName}" WHERE id = $1 RETURNING id::int, name, price::float, "submittedDate", "closedDate", description, increment::float, "startingPrice"::float, "sellerId"::int, "categoryId"::int;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  }
};
