const pool = require('../utils/db');

const tbName = 'Category';

module.exports = {
  all: async () => {
    const sql = `SELECT "${tbName}".*, cat2.name AS "rootName" FROM "${tbName}" LEFT JOIN "${tbName}" AS cat2 ON "${tbName}".root = cat2.id;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  addCategory: async (name, root) => {
    if (root) {
      const sql = `INSERT INTO "${tbName}" (name, root) VALUES ($1,$2) RETURNING id::int, name, root;`;
      const result = await pool.query(sql, [name, parseInt(root)]);
      return result.rows[0];
    } else {
      const sql = `INSERT INTO "${tbName}" (name) VALUES ($1) RETURNING id::int, name, root;`;
      const result = await pool.query(sql, [name]);
      return result.rows[0];
    }
  },
  updateCategory: async (id, name, root) => {
    if (root != 0) {
      const sql = `UPDATE "${tbName}" SET (name, root) = ($1,$2) WHERE id = $3 RETURNING id::int, name, root;`;
      const result = await pool.query(sql, [name, parseInt(root), parseInt(id)]);
      return result.rows[0];
    } else {
      const sql = `UPDATE "${tbName}" SET (name, root) = ($1, NULL) WHERE id = $2 RETURNING id::int, name, root;`;
      const result = await pool.query(sql, [name, parseInt(id)]);
      return result.rows[0];
    }
  },
  deleteCategory: async id => {
    const sql = `DELETE FROM "${tbName}" WHERE id = $1 RETURNING id::int, name, root;`;
    const result = await pool.query(sql, [parseInt(id)]);
    return result.rows[0];
  }
};
