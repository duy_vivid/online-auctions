const pool = require('../utils/db');

const tbName = 'Banlist';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}";`;
    const result = await pool.query(sql);
    return result.rows;
  },
  allByProductId: async productId => {
    const sql = `SELECT * FROM "${tbName}" WHERE "productId" = $1;`;
    const result = await pool.query(sql, [productId]);
    return result.rows;
  },
  checkBanned: async (userId, productId) => {
    const sql = `SELECT * FROM "${tbName}" WHERE "productId" = $1 AND "userId" = $2;`;
    const result = await pool.query(sql, [productId, userId]);
    return result.rows[0];
  },
  addItem: async (userId, productId) => {
    const sql = `INSERT INTO "${tbName}" ("userId", "productId") VALUES ($1, $2) RETURNING *;`;
    const result = await pool.query(sql, [userId, productId]);
    return result.rows[0];
  },
  deleteRequest: async (userId, productId) => {
    const sql = `DELETE FROM "${tbName}" WHERE "userId" = $1 AND "productId" = $2 RETURNING *;`;
    const result = await pool.query(sql, [userId, productId]);
    return result.rows[0];
  }
};
