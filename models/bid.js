const pool = require('../utils/db');

const tbName = 'Bid';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}" ORDER BY approved;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  allApprovedByBidderId: async bidderId => {
    const sql = `SELECT current, "maxBid"."productId", "myMax", name, "closedDate" FROM (SELECT MAX(value) AS current, "productId" FROM "${tbName}" WHERE approved = true GROUP BY "productId") AS "maxBid" JOIN (SELECT max(value) AS "myMax", "productId", name, "closedDate" FROM "${tbName}" JOIN "Product" ON "${tbName}"."productId" = "Product".id AND approved = true WHERE "bidderId" = $1 AND "winnerId" IS NULL GROUP BY "productId", name, "closedDate") AS "myBidding" ON "maxBid"."productId" = "myBidding"."productId";`;
    const result = await pool.query(sql, [parseInt(bidderId)]);
    return result.rows;
  },
  maxApprovedByProductId: async productId => {
    const sql = `SELECT * FROM "${tbName}" WHERE "productId" = $1 AND approved = true ORDER BY value DESC, date LIMIT 1;`;
    const result = await pool.query(sql, [parseInt(productId)]);
    return result.rows[0];
  },
  byId: async id => {
    const sql = `SELECT * FROM "${tbName}" WHERE id = $1;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  },
  byId: async id => {
    const sql = `SELECT * FROM "${tbName}" WHERE id = $1;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  },
  allByProductId: async productId => {
    const sql = `SELECT "Bid".*, "User".id AS "bidderId", "User".name AS "bidderName" FROM "${tbName}" JOIN "User" ON "Bid"."bidderId" = "User".id WHERE "productId" = $1 ORDER BY value, date;`;
    const result = await pool.query(sql, [productId]);
    return result.rows;
  },
  addBid: async (bidderId, productId, value) => {
    const now = new Date();
    const sql = `INSERT INTO "${tbName}" ("bidderId", "productId", value, date) VALUES ($1, $2, $3, $4) RETURNING id::int, "bidderId"::int, "productId"::int, value::float, date, approved;`;
    const result = await pool.query(sql, [bidderId, productId, value, now]);
    return result.rows[0];
  },
  updateBid: async (id, bidderId, productId, value, approved) => {
    const sql = `UPDATE "${tbName}" SET ("bidderId", "productId", value, approved) = ($1, $2, $3, $4) WHERE id = $5 RETURNING id::int, "bidderId"::int, "productId"::int, value::float, date, approved;`;
    const result = await pool.query(sql, [bidderId, productId, value, approved, id]);
    return result.rows[0];
  },
  updateBidAprroval: async (id, approved) => {
    const sql = `UPDATE "${tbName}" SET approved = $1 WHERE id = $2 RETURNING id::int, "bidderId"::int, "productId"::int, value::float, date, approved;`;
    const result = await pool.query(sql, [approved, id]);
    return result.rows[0];
  },
  updateAllBidApproval: async (bidderId, productId, approved) => {
    const sql = `UPDATE "${tbName}" SET approved = $1 WHERE "bidderId" = $2 AND "productId" = $3 RETURNING id::int, "bidderId"::int, "productId"::int, value::float, date, approved;`;
    const result = await pool.query(sql, [approved, bidderId, productId]);
    return result.rows;
  },
  updateAllBidApprovalWithMax: async (bidderId, productId, approved, value) => {
    const sql = `UPDATE "${tbName}" SET approved = $1 WHERE "bidderId" = $2 AND "productId" = $3 AND value <= $4 RETURNING id::int, "bidderId"::int, "productId"::int, value::float, date, approved;`;
    const result = await pool.query(sql, [approved, bidderId, productId, value]);
    return result.rows;
  },
  deleteBid: async id => {
    const sql = `DELETE FROM "${tbName}" WHERE "id" = $1 RETURNING *;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  }
};
