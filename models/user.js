const pool = require('../utils/db');

const tbName = 'User';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}";`;
    const result = await pool.query(sql);
    return result.rows;
  },
  allSimple: async () => {
    const sql = `SELECT id::int, email, name, role FROM "${tbName}";`;
    const result = await pool.query(sql);
    return result.rows;
  },
  byEmail: async email => {
    const sql = `SELECT id::int, email, password, name, address, role FROM "${tbName}" WHERE email = $1;`;
    const result = await pool.query(sql, [email]);
    return result.rows[0];
  },
  byId: async id => {
    const sql = `SELECT * FROM "${tbName}" WHERE id = $1;`;
    const result = await pool.query(sql, [parseInt(id)]);
    return result.rows[0];
  },
  byProductId: async productId => {
    const sql = `SELECT "User".id::int, email, "User".name, address, role FROM "User" JOIN "Product" ON "User".id = "Product"."sellerId" WHERE "Product".id = $1;`;
    const result = await pool.query(sql, [parseInt(productId)]);
    return result.rows[0];
  },
  addUser: async (name, email, password, address, role = 'bidder') => {
    const sql = `INSERT INTO "${tbName}" (email, password, name, address, role) VALUES ($1, $2, $3, $4, $5) RETURNING id::int, email, name, address, role;`;
    const result = await pool.query(sql, [email, password, name, address, role]);
    return result.rows[0];
  },
  updateUser: async (id, name, password, address) => {
    const sql = `UPDATE "${tbName}" SET (name, password, address) = ($1, $2, $3) WHERE id = $4 RETURNING id::int, email, name, address, role;`;
    const result = await pool.query(sql, [name, password, address, parseInt(id)]);
    return result.rows[0];
  },
  updateUserSimple: async (id, name, email, address) => {
    const sql = `UPDATE "${tbName}" SET (name, email, address) = ($1, $2, $3) WHERE id = $4 RETURNING id::int, email, name, address, role;`;
    const result = await pool.query(sql, [name, email, address, parseInt(id)]);
    return result.rows[0];
  },
  updateUserRole: async (id, role) => {
    const sql = `UPDATE "${tbName}" SET role = $1 WHERE id = $2 RETURNING id::int, email, name, address, role;`;
    const result = await pool.query(sql, [role, id]);
    return result.rows[0];
  }
};
