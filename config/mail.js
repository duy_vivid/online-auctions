const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.EMAIL_USERNAME,
    pass: process.env.EMAIL_PASSWORD
  }
});

const sendEmail = async (receiver, subject, content) => {
  const mailOptions = {
    from: process.env.EMAIL_USERNAME,
    to: receiver,
    subject,
    html: content
  };
  const result = await transporter.sendMail(mailOptions);
  return result;
};

module.exports = sendEmail;
