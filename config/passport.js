const LocalStrategy = require('passport-local');
const bcrypt = require('bcryptjs');

const user = require('../controllers/user');

module.exports = passport => {
  passport.use(
    new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
      user
        .getByEmail(email)
        .then(user => {
          if (!user) {
            return done(null, false, { message: 'Không tìm thấy email.' });
          }
          bcrypt.compare(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
              return done(null, user);
            } else {
              return done(null, false, { message: 'Sai mật khẩu.' });
            }
          });
        })
        .catch(err => console.log(err));
    })
  );

  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    user.getById(id).then(res => {
      done(null, res);
    });
  });
};
