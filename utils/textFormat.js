module.exports = {
  maskText: text => {
    let result = '';
    for (let i = 0; i < text.length; i++) {
      result += i < text.length / 2 - 1 ? '*' : text[i];
    }
    return result;
  },
  formatMoney: value => {
    return String(value).replace(/(.)(?=(\d{3})+$)/g, '$1.');
  }
};
