module.exports = {
  paginate: (perPage, page) => {
    return [perPage, (page - 1) * perPage];
  },
  order: orderInstr => {
    switch (orderInstr) {
      case 'count_desc':
        return 'ORDER BY counts DESC';
      case 'price_asc':
        return 'ORDER BY "lowestPossible"';
      case 'price_desc':
        return 'ORDER BY "lowestPossible" DESC';
      case 'remaining_time_asc':
        return 'ORDER BY "closedDate"';
    }
  }
};
