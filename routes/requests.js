const express = require('express');

const request = require('../controllers/request');
const user = require('../controllers/user');

const router = express.Router();

router
  .post('/', async (req, res) => {
    const requestRes = request.newRequest(parseInt(req.query.userId));
    return res.redirect('dashboard');
  })
  .post('/:userId', async (req, res) => {
    if (req.user.role === 'admin') {
      request.updateRequest(req.params.userId, req.body.approved);
      if (req.body.approved === 'true') {
        user.updateUserRole(req.params.userId, 'seller');
      }
      return res.redirect('/dashboard');
    } else {
      return res.status(403).send('Bạn không có quyền này.');
    }
  });

module.exports = router;
