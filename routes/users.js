const express = require('express');
const bcrypt = require('bcryptjs');
const passport = require('passport');

const user = require('../controllers/user');
const watchlist = require('../controllers/watchlist');
const categoryController = require('../controllers/category');

const { ensureAuthenticated } = require('../config/auth');

const router = express.Router();

// Login page
router.get('/login', async (req, res) => {
  const categoriesHeader = await categoryController.getCategories();
  return res.render('login', { categoriesHeader });
});

// Register page
router.get('/register', async (req, res) => {
  const categoriesHeader = await categoryController.getCategories();
  return res.render('register', { categoriesHeader });
});

// Register handle
router.post('/register', async (req, res) => {
  const { name, email, address, password, password2 } = req.body;
  let errors = [];
  // Check required fields
  if (!name || !email || !address || !password || !password2) {
    errors.push({ msg: 'Điền cho đầy đủ thông tin...' });
  }

  // Check passwords match
  if (password !== password2) {
    errors.push({ msg: 'Mật khẩu không trùng khớp...' });
  }

  // Check pass length
  if (password.length < 6) {
    errors.push({ msg: 'Mật khẩu có ít nhất 6 kí tự.' });
  }

  if (errors.length > 0) {
    res.render('register', {
      errors,
      name,
      email,
      address,
      password,
      password2
    });
  } else {
    // Validation passed
    const userByEmail = await user.getByEmail(email);
    if (userByEmail) {
      // User exists
      errors.push({ msg: 'Email đã được sử dụng.' });
      res.render('register', {
        errors,
        name,
        email,
        address,
        password,
        password2
      });
    } else {
      // Hash password
      bcrypt.genSalt(10, (err, salt) =>
        bcrypt.hash(password, salt, (err, hash) => {
          if (err) throw err;
          // Save user
          user.newUser(name, email, hash, address).then(user => {
            req.flash('success_msg', 'Đăng ký thành công!');
            res.redirect('/users/login');
          });
        })
      );
    }
  }
});

router
  .post('/login', (req, res, next) => {
    passport.authenticate('local', {
      successRedirect: '/dashboard',
      failureRedirect: '/users/login',
      failureFlash: true
    })(req, res, next);
  })
  .post('/:id', ensureAuthenticated, async (req, res) => {
    const { name, address, password, password2, currentPassword } = req.body;
    let errors = [];
    // Check required fields
    if (!currentPassword) {
      errors.push({ msg: 'Mật khẩu cũ không được bỏ trống...' });
    }
    // Check passwords match
    if (password !== password2) {
      errors.push({ msg: 'Mật khẩu không trùng khớp...' });
    }
    // Check pass length
    if (password.length < 6) {
      errors.push({ msg: 'Mật khẩu có ít nhất 6 kí tự.' });
    }
    if (errors.length > 0) {
      return res.redirect('/dashboard');
    } else {
      const userById = await user.getById(parseInt(req.params.id));
      bcrypt.compare(currentPassword, userById.password, (err, isMatch) => {
        if (err) throw err;
        if (isMatch) {
          bcrypt.genSalt(10, (err, salt) =>
            bcrypt.hash(password, salt, (err, hash) => {
              if (err) throw err;
              // Save user
              user
                .updateUser(
                  parseInt(req.params.id),
                  name ? name : req.user.name,
                  password ? hash : userById.password,
                  address ? address : req.user.address
                )
                .then(user => {
                  return res.redirect('/dashboard');
                });
            })
          );
        } else {
          req.flash('error_msg', 'Sai mật khẩu!');
          return res.redirect('/dashboard');
        }
      });
    }
  })
  .post('/:id/edit', ensureAuthenticated, async (req, res) => {
    if (req.user.role !== 'admin') {
      return res.redirect('/dashboard');
    }
    const updateUser = await user.updateUserSimple(req.params.id, req.body.name, req.body.email, req.body.address);
    req.flash('success_msg', 'Bạn đã chỉnh sửa thành viên thành công!');
    return res.redirect('/dashboard');
  });

// Logout handle
router.get('/logout', (req, res) => {
  req.logOut();
  req.flash('success_msg', 'Bạn đã đăng xuất!');
  res.redirect('/users/login');
});

router
  .get('/:id/watchlist/:productId', ensureAuthenticated, async (req, res) => {
    const { id, productId } = req.params;
    if (req.query.add == 'true') {
      await watchlist.addItem(id, productId);
      return res.redirect(`/products/${productId}`);
    } else {
      await watchlist.removeItem(id, productId);
      return res.redirect(`/products/${productId}`);
    }
  })
  .get('/:id/edit', ensureAuthenticated, async (req, res) => {
    if (req.user.role !== 'admin') return res.redirect('/dashboard');
    const [userInfo, categoriesHeader] = await Promise.all([
      user.getById(req.params.id),
      categoryController.getCategories()
    ]);
    return res.render('userEdit', { userInfo, categoriesHeader });
  });

module.exports = router;
