const express = require('express');

const category = require('../controllers/category');
const product = require('../controllers/product');
const bid = require('../controllers/bid');
const banlist = require('../controllers/banlist');
const watchlist = require('../controllers/watchlist');
const user = require('../controllers/user');

const sendEmail = require('../config/mail');
const textFormat = require('../utils/textFormat');
const { ensureAuthenticated } = require('../config/auth');

const PER_PAGE = 6;

const router = express.Router();

router
  .get('/', async (req, res) => {
    let activeCategory = 1,
      order = 'count_desc',
      page = 1,
      user = { id: 0 },
      categoryList = [],
      keywords = '';
    if (typeof req.query.category !== 'undefined') activeCategory = parseInt(req.query.category);
    if (typeof req.query.order !== 'undefined') order = req.query.order;
    if (typeof req.query.page !== 'undefined') page = parseInt(req.query.page);
    if (typeof req.user !== 'undefined') user = req.user;
    if (typeof req.query.keywords !== 'undefined') keywords = req.query.keywords;
    categoryList.push(activeCategory);
    const categories = await category.getCategories();
    for (let i = 0; i < categories.length; i++) {
      if (categories[i].id === activeCategory) {
        categories[i].childs.forEach(child => categoryList.push(child.id));
        break;
      }
    }
    let products, userWatchlist;
    if (keywords === '') {
      [products, userWatchlist] = await Promise.all([
        product.getProductsByCategoryId(categoryList, order, page, PER_PAGE),
        watchlist.getWatchlistByUserIdSimple(user.id)
      ]);
    } else {
      [products, userWatchlist] = await Promise.all([
        product.searchProducts(categoryList, order, page, PER_PAGE, keywords),
        watchlist.getWatchlistByUserIdSimple(user.id)
      ]);
    }
    const totalPages = products.length ? Math.ceil(products[0].full_count / PER_PAGE) : 1;
    return res.render('products', {
      user,
      userWatchlist,
      keywords,
      categories,
      activeCategory,
      order,
      products,
      textFormat,
      totalPages,
      page,
      categoriesHeader: categories
    });
  })
  .get('/new', ensureAuthenticated, async (req, res) => {
    if (req.user.role === 'seller' || req.user.role === 'admin') {
      const [categories, categoriesHeader] = await Promise.all([category.getAllCategories(), category.getCategories()]);
      return res.render('newProduct', { categories, categoriesHeader });
    } else {
      req.flash('error_msg', 'Bạn không có tài khoản người bán.');
      return res.redirect('/dashboard');
    }
  })
  .get('/:id', async (req, res) => {
    let user = { id: 0 };
    if (typeof req.user !== 'undefined') user = req.user;
    const [returnedProduct, bids, userWatchlist, categoriesHeader] = await Promise.all([
      product.getProductById(req.params.id),
      bid.getBidsByProductId(req.params.id),
      watchlist.getWatchlistByUserIdSimple(user.id),
      category.getCategories()
    ]);
    let approvedBid = bids.filter(bid => bid.approved);
    let highestBid = approvedBid.length > 0 ? approvedBid.reduce((a, b) => (a.value > b.value ? a : b)) : null;
    return res.render('productDetail', {
      product: returnedProduct,
      userWatchlist,
      highestBid,
      bids,
      user,
      textFormat,
      categoriesHeader
    });
  })
  .get('/:id/bids/:bidId', ensureAuthenticated, async (req, res) => {
    const bidItem = await bid.getBidById(req.params.bidId);
    if (req.query.approved == 'true') {
      bid.approveAllBidWithMax(bidItem.bidderId, bidItem.productId, bidItem.value);
      Promise.all([
        user.getById(bidItem.bidderId),
        product.getProductNameById(bidItem.productId)
      ]).then(([recipient, prod]) =>
        sendEmail(recipient.email, 'Đấu giá thành công', `Phiên đấu giá sản phẩm ${prod.name} của bạn đã được duyệt.`)
      );
    } else {
      bid.disapproveAllBid(bidItem.bidderId, bidItem.productId);
      Promise.all([
        user.getById(bidItem.bidderId),
        product.getProductNameById(bidItem.productId)
      ]).then(([recipient, prod]) =>
        sendEmail(
          recipient.email,
          'Đấu giá thất bại',
          `Phiên đấu giá sản phẩm ${prod.name} của bạn đã bị từ chối và bạn không thể tiếp tục tham gia đấu giá sản phẩm này.`
        )
      );
      banlist.banBidder(bidItem.bidderId, bidItem.productId);
    }
    return res.redirect(`/products/${req.params.id}`);
  })
  .get('/:id/edit', ensureAuthenticated, async (req, res) => {
    const [productItem, categoriesHeader] = await Promise.all([
      product.getProductByIdSimple(req.params.id),
      category.getCategories()
    ]);
    if (req.user.id != productItem.sellerId) return res.redirect('/dashboard');
    return res.render('addDescription', { productItem, categoriesHeader });
  });

router
  .post('/', ensureAuthenticated, async (req, res) => {
    if (req.user.role === 'seller' || req.user.role === 'admin') {
      const { name, startingPrice, increment, price, category, description } = req.body;
      const closedDate = new Date(`${req.body.closedDate} ${req.body.closedTime}`);
      const image = new Buffer(req.files.image.data).toString('base64');
      const sideImages = req.files.sideImages.map(img => new Buffer(img.data).toString('base64'));
      const newProduct = await product.newProduct(
        name,
        image,
        sideImages,
        price,
        closedDate,
        description,
        increment,
        startingPrice,
        req.user.id,
        category
      );
      return res.redirect(`/products/${newProduct.id}`);
    }
  })
  .post('/:id/bids', ensureAuthenticated, async (req, res) => {
    const returnedBid = await bid.addBid(req.user.id, req.params.id, req.body.value);
    if (returnedBid.err) {
      req.flash('error_msg', returnedBid.err);
      return res.redirect(`/products/${req.params.id}`);
    }
    Promise.all([
      user.getByProductId(req.params.id),
      user.getById(req.user.id),
      product.getProductNameById(req.params.id)
    ]).then(([seller, bidder, prod]) =>
      sendEmail(
        seller.email,
        `Tin đấu giá sản phẩm ${prod.name}`,
        `${bidder.name} vừa ra giá ${req.body.value}đ cho sản phẩm ${prod.name} của bạn.`
      )
    );
    req.flash('success_msg', 'Lượt đấu giá của bạn đang chờ được xác nhận.');
    return res.redirect(`/products/${req.params.id}`);
  })
  .post('/:id/buy', ensureAuthenticated, async (req, res) => {
    const returnedRes = await product.setWinner(req.params.id, req.user.id, req.body.finalPrice);
    if (returnedRes.err) {
      req.flash('error_msg', returnedRes.err);
      return res.redirect(`/products/${req.params.id}`);
    }
    Promise.all([
      user.getByProductId(req.params.id),
      user.getById(req.user.id),
      product.getProductNameById(req.params.id)
    ]).then(([seller, bidder, prod]) => {
      sendEmail(
        seller.email,
        `Tin đấu giá sản phẩm ${prod.name}`,
        `${bidder.name} vừa mua sản phẩm ${prod.name} của bạn.`
      );
      sendEmail(bidder.email, `Tin đấu giá sản phẩm ${prod.name}`, `Bạn vừa mua sản phẩm ${prod.name} thành công.`);
    });
    req.flash('success_msg', 'Bạn đã mua sản phẩm thành công.');
    return res.redirect(`/products/${req.params.id}`);
  })
  .post('/:id', ensureAuthenticated, async (req, res) => {
    if (req.user.role !== 'seller') return res.redirect('/dashboard');
    const productItem = await product.getProductByIdSimple(req.params.id);
    if (req.user.id != productItem.sellerId) return res.redirect('/dashboard');
    const newDescription = `${productItem.description}\n${req.body.description}`;
    const updatedProduct = await product.updateDescription(req.params.id, newDescription);
    return res.redirect(`/products/${req.params.id}`);
  });

module.exports = router;
