const express = require('express');

const categoryController = require('../controllers/category');

const { ensureAuthenticated } = require('../config/auth');

const router = express.Router();

router
  .get('/:id/edit', ensureAuthenticated, async (req, res) => {
    if (req.user.role !== 'admin') return res.redirect('/dashboard');
    const [allCategories, categories] = await Promise.all([
      categoryController.getAllCategories(),
      categoryController.getCategories()
    ]);
    const currentCategory = allCategories.find(cat => cat.id == req.params.id);
    if (!currentCategory.root) currentCategory.root = 0;
    return res.render('categoryEdit', { currentCategory, categories, categoriesHeader: categories });
  })
  .get('/:id/delete', ensureAuthenticated, async (req, res) => {
    if (req.user.role !== 'admin') return res.redirect('/dashboard');
    const deletedCategory = await categoryController.deleteCategory(req.params.id);
    return res.redirect('/dashboard');
  })
  .get('/new', ensureAuthenticated, async (req, res) => {
    if (req.user.role !== 'admin') return res.redirect('/dashboard');
    const categories = await categoryController.getCategories();
    return res.render('newCategory', { categories, categoriesHeader: categories });
  });

router
  .post('/:id/edit', ensureAuthenticated, async (req, res) => {
    if (req.user.role !== 'admin') return res.redirect('/dashboard');
    const updatedCategory = await categoryController.updateCategory(req.params.id, req.body.name, req.body.root);
    return res.redirect('/dashboard');
  })
  .post('/', ensureAuthenticated, async (req, res) => {
    if (req.user.role !== 'admin') return res.redirect('/dashboard');
    const addedCategory = await categoryController.addCategory(req.body.name, req.body.root);
    return res.redirect('/dashboard');
  });

module.exports = router;
