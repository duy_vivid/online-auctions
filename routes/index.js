const express = require('express');

const request = require('../controllers/request');
const product = require('../controllers/product');
const bid = require('../controllers/bid');
const userC = require('../controllers/user');
const categoryC = require('../controllers/category');
const { ensureAuthenticated } = require('../config/auth');
const formatText = require('../utils/textFormat');

const router = express.Router();

// Welcome page
router.get('/', async (req, res) => {
  const [categoriesHeader, topCountsProducts, topValueProducts, topEndingProducts] = await Promise.all([
    categoryC.getCategories(),
    product.getTopProducts('count_desc', 5),
    product.getTopProducts('price_desc', 5),
    product.getTopProducts('remaining_time_asc', 5)
  ]);
  return res.render('welcome', {
    user: req.user,
    categoriesHeader,
    topCountsProducts,
    topEndingProducts,
    topValueProducts,
    formatText
  });
});
// Dashboard
router.get('/dashboard', ensureAuthenticated, async (req, res) => {
  const { user } = req;
  let requests = [],
    requestsList = [],
    boughtProducts = [],
    biddings = [],
    sellingProducts = [],
    soldProducts = [],
    userList = [],
    categoryList = [],
    categoriesHeader = [];
  switch (user.role) {
    case 'admin':
      [requestsList, boughtProducts, biddings, userList, categoryList, categoriesHeader] = await Promise.all([
        request.getUnapprovedRequests(),
        product.getProductByWinnerId(user.id),
        bid.getApprovedByBidderId(user.id),
        userC.getAllSimple(),
        categoryC.getAllCategories(),
        categoryC.getCategories()
      ]);
      break;
    case 'seller':
      [boughtProducts, biddings, sellingProducts, soldProducts, categoriesHeader] = await Promise.all([
        product.getProductByWinnerId(user.id),
        bid.getApprovedByBidderId(user.id),
        product.getProductsBySellerId(user.id),
        product.getSoldProductsBySellerId(user.id),
        categoryC.getCategories()
      ]);
      break;
    case 'bidder':
      [requests, boughtProducts, biddings, categoriesHeader] = await Promise.all([
        request.getRequestsByUserId(user.id),
        product.getProductByWinnerId(user.id),
        bid.getApprovedByBidderId(user.id),
        categoryC.getCategories()
      ]);
      break;
  }
  return res.render('dashboard', {
    user,
    requests,
    requestsList,
    boughtProducts,
    formatText,
    biddings,
    sellingProducts,
    soldProducts,
    userList,
    categoryList,
    categoriesHeader
  });
});

module.exports = router;
